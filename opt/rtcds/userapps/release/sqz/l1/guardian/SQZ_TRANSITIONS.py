from guardian import GuardState, GuardStateDecorator
import subprocess, os, signal, atexit

nominal = 'IDLE'

# define channel names and constants
## GENERAL
GLITCHMON_HEARTBEAT = 'SQZ-GLITCHMON_HEARTBEAT'
GLITCHMON_HD_TIME = 'SQZ-GLITCHMON_HD_TIME'
GLITCHMON_IFO_TIME = 'SQZ-GLITCHMON_IFO_TIME'

## OPTIMIZE_NLG
## optimize non-linear gain with temperature
LOCK_IN_NLG = 'SQZ-OPO_GRD_LOCK_IN_NLG_MODE'

OPO_TEMP = 'SQZ-OPO_TEC_SETTEMP'
CLF2_DEMOD = 'SQZ-CLF_REFL_RF6_DEMOD_RFMON'
HD_PDA = 'SQZ-HD_A_DC_OUT16'

DT = 'SQZ-GRD_SETTINGS_OPTNLG_DT'
DT_MAX = 'SQZ-GRD_SETTINGS_OPTNLG_MAX_DT'
SETTLE_TIME = 'SQZ-GRD_SETTINGS_OPTNLG_SETTLE_TIME'
AVERAGE_TIME = 'SQZ-GRD_SETTINGS_OPTNLG_AVG_TIME'

## dither scripts
## TODO: test all the dither scripts

SCRIPT_PATH = '/opt/rtcds/userapps/release/sqz/l1/scripts'
ZM1_SCRIPT = os.path.join(SCRIPT_PATH, 'dither_ZM1.py')
ZMS_SCRIPT = os.path.join(SCRIPT_PATH, 'dither_ZMs.py')
SQZANG_HD_SCRIPT = os.path.join(SCRIPT_PATH, 'dither_SQZANG_HD.py')
SQZANG_OPTIM_SCRIPT = os.path.join(SCRIPT_PATH, 'optim_SQZANG_HD.py')
TEST_CONNECT_SCRIPT = os.path.join(SCRIPT_PATH, 'test_connect.py')
SQZANG_IFO_SCRIPT = os.path.join(SCRIPT_PATH, 'dither_SQZANG_OMC.py')
# SQZANG_OPTIM_IFO_SCRIPT = os.path.join(SCRIPT_PATH, 'optim_SQZANG_IFO.py')

## channels to revert with burt
SNAP_PATH = '/opt/rtcds/userapps/release/sqz/l1/burtfiles/l1sqz_transitions.snap'
RECOVER_CHANNELS = ['SUS-ZM1_M1_OPTICALIGN_P_OFFSET',
                    'SUS-ZM1_M1_OPTICALIGN_Y_OFFSET',
                    'SUS-ZM2_M1_OPTICALIGN_P_OFFSET',
                    'SUS-ZM2_M1_OPTICALIGN_Y_OFFSET',
                    'SQZ-HD_DIFF_RF3_PHASE_PHASEDEG',
                    'SQZ-OMC_TRANS_RF3_PHASE_PHASEDEG',
                    'SQZ-CLF_REFL_RF6_PHASE_PHASEDEG']

#######################
# SUBPROCESS HANDLING
#######################

class TransitionsProcess:
    process = None

    def start_process(self, path):
        save_alignment()
        print(os.environ)
        self.process = subprocess.Popen(['python', path])

    def kill_process(self):
        # if process is running, kill and restore alignment
        if self.process is not None and self.process.poll() is None:
            os.kill(self.process.pid, signal.SIGINT)
            self.process = None
            restore_alignment()

    def check_done(self):
        done = (self.process is not None and self.process.poll() is not None)
        if done:
            save_alignment()
            return True

shared = TransitionsProcess()
#atexit.register(shared.kill_process)
# TODO: figure out how to restore when this guardian dies.

#######################
# HELPER FUNCTIONS
#######################

def notify_log(msg):
    notify(msg)
    log(msg)

def save_alignment():
    with open(SNAP_PATH, 'w') as file:
        for key in ezca.setpoints:
            file.write('%s 1 %d\n' % (ezca.setpoints[key][0], ezca[key]))
            # TODO: fix this to use ezca functions

def restore_alignment():
    if os.path.isfile(SNAP_PATH):
        # fix namespace problem if transition guardian
        # is killed and this function is called by
        # atexit.
        try:
            ezca.burtwb(SNAP_PATH)
	except:
            from ezca import Ezca
            Ezca().export()
            ezca.burtwb(SNAP_PATH)
    else:
        notify_log('No alignment snapshot found')

#######################
# STATES
#######################

class INIT(GuardState):
    index = 0
    request = True

class IDLE(GuardState):
    index = 1
    request = True
    goto = True

    def main(self):
        if len(ezca.setpoints) != len(RECOVER_CHANNELS):
            ezca.init_setpoints(RECOVER_CHANNELS)
        # no subprocesses should be running in idle
	shared.kill_process()
        return True

class RESTORE(GuardState):
    index = 2
    request = True

    def main(self):
        restore_alignment()

#######################
# OPTIMIZE NLG
#######################

class OPTIMIZE_NLG_RUNNING(GuardState):
    index = 3
    request = False

    def main(self):
        self.startTemp = ezca[OPO_TEMP]
        self.finalTemp = self.startTemp - ezca[DT_MAX]

        # keep track of temperature and non-linear gains observed
        self.temps = []
        self.NLGavgs = []

        self.wait = False
        self.measure = False

        self.nlgReadback = CLF2_DEMOD if ezca[LOCK_IN_NLG] else HD_PDA

    def run(self):
        if self.wait:
            # start measuring when finished waiting for temperature to settle
            if self.timer['wait']:
                self.wait = False
                self.measure = True

                self.timer['measure'] = ezca[AVERAGE_TIME]
                self.runningNLG = []
        elif self.measure:
            # if measurement time elapsed, record average temperature
            # and step
            if self.timer['measure']:
                self.temps += [ezca[OPO_TEMP]]
                self.NLGavgs += [ sum(self.runningNLG) / len(self.runningNLG) ]

                ezca[OPO_TEMP] -= ezca[DT]

                self.measure = False
            # while measuring, store another NLG value
            else:
                self.runningNLG += [ezca[self.nlgReadback]]
        else:
            # between measurements, check if the temperature change
            # has exceeded the allowed value,
            # and check whether the last two NLG records have been decreasing
            if ezca[OPO_TEMP] <= self.finalTemp or (len(self.temps) > 2 and self.NLGavgs[-1] < self.NLGavgs[-2] and self.NLGavgs[-2] < self.
NLGavgs[-3]):
                optimalTemp = self.temps[max(range(len(self.NLGavgs)),
                                             key=lambda x: self.NLGavgs[x])]
                ezca[OPO_TEMP] = optimalTemp
                if optimalTemp == self.temps[0]:
                    notify_log('Starting temperature was optimal (may need to lock on higher T)')
                return True
            else:
                self.timer['wait'] = ezca[SETTLE_TIME]
                self.wait = True

class OPTIMIZE_NLG(GuardState):
    index = 4
    request = True


#######################
# DITHER ZM1
#######################

class DITHER_ZM1_RUNNING(GuardState):
    index = 5
    request = False

    def main(self):
        shared.start_process(ZM1_SCRIPT)

    def run(self):
        return shared.check_done()

class DITHER_ZM1(GuardState):
    index = 6
    request = True

#######################
# DITHER ZMs
#######################

class DITHER_ZMS_RUNNING(GuardState):
    index = 7
    request = False

    def main(self):
        shared.start_process(ZMS_SCRIPT)

    def run(self):
        return shared.check_done()

class DITHER_ZMS(GuardState):
    index = 8
    request = True

#######################
# DITHER SQZANG HD
#######################

class DITHER_SQZANG_HD_RUNNING(GuardState):
    index = 9
    request = False

    def main(self):
        shared.start_process(SQZANG_HD_SCRIPT)

    def run(self):
        return shared.check_done()

class DITHER_SQZANG_HD(GuardState):
    index = 10
    request = True


#######################
# DITHER SQZANG IFO
#######################

class DITHER_SQZANG_IFO_RUNNING(GuardState):
    index = 11
    request = False

    def main(self):
            shared.start_process(SQZANG_IFO_SCRIPT)

    def run(self):
        return shared.check_done()

class DITHER_SQZANG_IFO(GuardState):
    index = 12
    request = True


class TEST_CONNECT_RUN(GuardState):
    request = False

    def main(self):
        shared.start_process(TEST_CONNECT_SCRIPT)

    def run(self):
        return shared.check_done()

class TEST_CONNECT(GuardState):
    request = True

#######################
# OPTIMIZE SQZANG 
# Can be used both for HD and OMC 
#######################

class OPTIM_SQZANG_RUNNING(GuardState):
    index = 13
    request = False

    def main(self):
        shared.start_process(SQZANG_OPTIM_SCRIPT)

    def run(self):
        return shared.check_done()

class OPTIM_SQZANG(GuardState):
    index = 14
    request = True


#######################
# GRAPH
#######################

edges = [
    ('IDLE', 'RESTORE'),

    ('IDLE', 'OPTIMIZE_NLG_RUNNING'),
    ('OPTIMIZE_NLG_RUNNING', 'OPTIMIZE_NLG'),
    ('OPTIMIZE_NLG', 'IDLE'),

    ('IDLE', 'DITHER_ZM1_RUNNING'),
    ('DITHER_ZM1_RUNNING', 'DITHER_ZM1'),
    ('DITHER_ZM1', 'IDLE'),

    ('IDLE', 'DITHER_ZMS_RUNNING'),
    ('DITHER_ZMS_RUNNING', 'DITHER_ZMS'),
    ('DITHER_ZMS', 'IDLE'),

    ('IDLE', 'DITHER_SQZANG_HD_RUNNING'),
    ('DITHER_SQZANG_HD_RUNNING', 'DITHER_SQZANG_HD'),
    ('DITHER_SQZANG_HD', 'IDLE'),

    ('IDLE', 'DITHER_SQZANG_IFO_RUNNING'),
    ('DITHER_SQZANG_IFO_RUNNING', 'DITHER_SQZANG_IFO'),
    ('DITHER_SQZANG_IFO', 'IDLE'),

    ('IDLE', 'OPTIM_SQZANG_RUNNING'),
    ('OPTIM_SQZANG_RUNNING', 'OPTIM_SQZANG'),
    ('OPTIM_SQZANG', 'IDLE'),

    ('IDLE', 'TEST_CONNECT_RUN'),
    ('TEST_CONNECT_RUN', 'TEST_CONNECT'),
    ('TEST_CONNECT', 'IDLE'),
    ]
